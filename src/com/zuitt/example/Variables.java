package com.zuitt.example;

public class Variables {
    public static void main(String[] args) {
        // Variable Declaration
        int age;
        char middle_name;

        // Variable Declaration vs Initialization
        int x;
        int y = 0;

        System.out.println("The Value of y is " + y);

        y = 10;
        System.out.println("The Value of y is " + y);
    }
}
