package com.zuitt.example;
import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {

        // Create a Scanner Object
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter a username: ");

        // Read User Input
        String userName = myObj.nextLine();
        System.out.println("Username is: " + userName);
    }
}
